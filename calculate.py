import numpy as np
from scipy.optimize import minimize
import matplotlib.pyplot as pl

# Read in data (currently only x)
file = open('01-x-data.txt', 'r')

time1 = []
x1 = []

for line in file:
    tem = line.split()
    tem2 = tem[0].split('/')
    temp = tem[1].split(',')
    temp2 = temp[0].split(':')
    # Calculate time (ignoring year and month)
    timet = 24*3600*float(tem2[2])+3600*float(temp2[0]) + 60*float(temp2[1]) + float(temp2[2])
    time1.append(timet)
    x1.append(float(temp[1]))

# Normalise time data t=[0,1]
time = np.asarray(time1)
time = time - time[0]
time = time/time[-1]

# Normalise x data x(0) = 0, x(1) = 1
x = np.asarray(x1)
xp = np.asarray(x1)
x = x - x[0]
x = x/x[-1]


# Calculate cubic Bezier function, P0 = 0 and P3 = 1 (given points)
def cubic_bezier(t, p1, p2):
    p0 = 0
    p3 = 1
    return ((1-t)**3)*p0 + (3*(1-t)**2)*t*p1 + (3*(1-t)*t**2)*p2 + p3*t**3


# Function that calculates error between cubic Bezier function with given P1 and P2 vs data
def comps(p):
    p1 = p[0]
    p2 = p[1]

    # x limited to [0,1] so p1 and p2 limited to [0,1]
    if p1 > 1 or p1 < 0 or p2 > 1 or p2 < 0:
        return 10**12

    fit = np.zeros(len(x))
    for i in range(0, len(x)):
        fit[i] = cubic_bezier(time[i], p1, p2)

    return np.sum(np.abs(x-fit))


# Set initial guess
init = [0, 1]
inits = np.asarray(init)
# Minimise value from comps by altering P1 and P2
res = minimize(comps, inits, method='nelder-mead',
                           options={'xtol': 1e-8, 'maxiter': 1000, 'disp': False})

print(res.x)

# Plot normalised data
pl.plot(time, x, label='Data')
# Plot result
pl.plot(time, cubic_bezier(time, res.x[0], res.x[1]), label='Fit')
# Plot expected result
pl.plot(time, cubic_bezier(time, 0.0, 1.0), label='Expected')
pl.xlim((0, 1))
# pl.ylim((0, 1))
pl.legend()
pl.show()
