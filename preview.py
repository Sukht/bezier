import sys
import numpy as np
import matplotlib
matplotlib.use('Qt5Agg')
from PyQt5.QtWidgets import QApplication, QMainWindow, QVBoxLayout, QSizePolicy, QWidget,\
    QPushButton, QHBoxLayout, QLineEdit, QLabel
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure


# Calculate quadratic Bezier function
def quad_bezier(t, p0, p1, p2):
    return (1-t)*((1-t)*p0 + t*p1) + t*((1-t)*p1 + t*p2)


# Calculate cubic Bezier function
def cubic_bezier(t, p0, p1, p2, p3):
    return (1-t)*quad_bezier(t, p0, p1, p2) + t*quad_bezier(t, p1, p2, p3)


class App(QMainWindow):
    def __init__(self):
        super().__init__()
        self.main_widget = QWidget(self)
        # Set initial values P1=(0, 0) and P2=(1, 1)
        self.p1x = "0.0"
        self.p1y = "0.0"
        self.p2x = "1.0"
        self.p2y = "1.0"
        # Construct GUI
        hbox1 = QHBoxLayout()
        hbox1.addStretch(1)
        vbox1 = QVBoxLayout()
        vbox1.addStretch(1)
        # Create plot button
        btn1 = QPushButton('Plot', self)
        btn1.clicked.connect(self.plot)
        # Create plot canvas
        self.m1 = PlotCanvas(self.main_widget, width=5, height=4)
        toolbar1 = NavigationToolbar(self.m1, self.main_widget)
        # Create text boxes that take in arguments for P1 and P2
        self.lbx1 = QLabel()
        self.lbx1.setText('P1-X')
        self.x1 = QLineEdit(self)
        self.x1.setText(str(self.p1x))
        self.x1.textChanged[str].connect(self.on_changedx1)
        self.lby1 = QLabel()
        self.lby1.setText('P1-Y')
        self.y1 = QLineEdit(self)
        self.y1.setText(str(self.p1y))
        self.y1.textChanged[str].connect(self.on_changedy1)
        self.lbx2 = QLabel()
        self.lbx2.setText('P2-X')
        self.x2 = QLineEdit(self)
        self.x2.setText(str(self.p2x))
        self.x2.textChanged[str].connect(self.on_changedx2)
        self.lby2 = QLabel()
        self.lby2.setText('P2-Y')
        self.y2 = QLineEdit(self)
        self.y2.setText(str(self.p2y))
        self.y2.textChanged[str].connect(self.on_changedy2)

        # Construct layout
        hbox1.addWidget(self.lbx1)
        hbox1.addWidget(self.x1)
        hbox1.addWidget(self.lby1)
        hbox1.addWidget(self.y1)
        hbox1.addWidget(self.lbx2)
        hbox1.addWidget(self.x2)
        hbox1.addWidget(self.lby2)
        hbox1.addWidget(self.y2)
        vbox1.addWidget(btn1)
        vbox1.addLayout(hbox1)
        vbox1.addWidget(self.m1)
        vbox1.addWidget(toolbar1)

        self.main_widget.setLayout(vbox1)
        self.main_widget.setFocus()
        self.setCentralWidget(self.main_widget)

        self.initui()

    def initui(self):
        self.setWindowTitle('Preview')
        self.show()

    # Allow text changes to change values of P1 and P2
    def on_changedx1(self, text):
        self.p1x = text

    def on_changedy1(self, text):
        self.p1y = text

    def on_changedx2(self, text):
        self.p2x = text

    def on_changedy2(self, text):
        self.p2y = text

    def plot(self):
        catch = False
        # Check that all values are floats
        try:
            float(self.p1x)
            float(self.p2x)
            float(self.p1y)
            float(self.p2y)
        except:
            catch = True
            print('Non float argument')

        # Calculate cubic Bezier and plot it
        if not catch:
            time = np.linspace(0, 1, 1000)
            x = cubic_bezier(time, 0.0, float(self.p1x), float(self.p2x), 1.0)
            y = cubic_bezier(time, 0.0, float(self.p1y), float(self.p2y), 1.0)
            self.m1.clf()
            self.m1.plot(x, y, xL='X', yL='Y', xl=[0, 1], yl=[0, 1])


# Setup canvas class to allow for plots in GUI
class PlotCanvas(FigureCanvas):

    def __init__(self, parent=None, width=5, height=4, dpi=100):
        fig = Figure(figsize=(width, height), dpi=dpi)
        self.axes = fig.add_subplot(111)

        FigureCanvas.__init__(self, fig)
        self.setParent(parent)

        FigureCanvas.setSizePolicy(self, QSizePolicy.Expanding, QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)

    # Setup plot function for plotting on the canvas
    def plot(self, x, y, L=None, xL=None, yL=None, xl=None, yl=None):
        ax = self.figure.add_subplot(111)
        if not L:
            ax.plot(x, y)
        else:
            ax.plot(x, y, label=L)
            ax.legend(loc='upper right')
        if xL:
            ax.set_xlabel(xL)
        if yL:
            ax.set_ylabel(yL)
        if xl:
            ax.set_xlim(xl)
        if yl:
            ax.set_ylim(yl)
        self.draw()

    # Function for clearing canvas
    def clf(self):
        self.figure.clf()
        self.draw()


# Initiate app
if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())